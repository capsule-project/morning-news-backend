from locust import HttpUser, task, between

class MyUser(HttpUser):
    wait_time = between(1, 5)
    host = "http://localhost:9001"

    @task
    def my_task(self):
        self.client.get("/")
