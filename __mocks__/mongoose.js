// __mocks__/mongoose.js
class Schema {
    constructor(definition) {
      this.definition = definition;
    }
  }
  
  class Model {
    constructor(data) {
      Object.assign(this, data);
      this._id = this._id || 'mocked_id';
    }
  
    static deleteOne(query) {
      return Promise.resolve({ deletedCount: 1 });
    }
  
    static updateOne(query, update) {
      return Promise.resolve({ n: 1, nModified: 1, ok: 1 });
    }
  
    static findOne(query) {
      return Promise.resolve(new Model({
        username: 'test-lacapsule',
        password: 'test123',
        token: 'faketoken',
        canBookmark: true,
        _id: 'mocked_id'
      }));
    }
  
    save() {
      return Promise.resolve(this);
    }
  }
  
  const mongoose = {
    connect: jest.fn().mockResolvedValue(() => {
      console.log("Mocked Mongoose connection established");
    }),
    connection: {
      close: jest.fn().mockResolvedValue(() => {
        console.log("Mocked Mongoose connection closed");
      }),
    },
    Schema,
    model: jest.fn().mockImplementation(() => Model),
  };
  
  module.exports = mongoose;
  