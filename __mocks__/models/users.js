// __mocks__/User.js
const User = function(data) {
    this.username = data.username;
    this.password = data.password;
    this.token = data.token;
    this.canBookmark = data.canBookmark;
    this._id = 'mocked_id';
  };
  
  User.deleteOne = jest.fn().mockResolvedValue({ deletedCount: 1 });
  User.updateOne = jest.fn().mockResolvedValue({ n: 1, nModified: 1, ok: 1 });
  User.findOne = jest.fn().mockResolvedValue(null);
  User.prototype.save = jest.fn().mockResolvedValue({
    _id: 'mocked_id',
    username: 'test-lacapsule',
    password: 'test123',
    token: 'faketoken',
    canBookmark: true,
  });
  
  module.exports = User;
  