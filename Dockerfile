#Utiliser une image officielle de Node.js comme image de base
FROM node:14

#Définir le répertoire de travail dans le conteneur
WORKDIR /app

#Copier le fichier package.json et package-lock.json
COPY package*.json ./

#Installer les dépendances
RUN npm install

#Copier le reste des fichiers de l'application
COPY . .

#Exposer le port de l'application
EXPOSE 3000

#Définir la commande de démarrage de l'application
CMD ["npm", "start"]

# Télécharge et installe SonarScanner
RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.2.2472-linux.zip \
    && unzip sonar-scanner-cli-4.6.2.2472-linux.zip \
    && mv sonar-scanner-4.6.2.2472-linux /opt/sonar-scanner \
    && ln -s /opt/sonar-scanner/bin/sonar-scanner /usr/local/bin/sonar-scanner \
    && rm sonar-scanner-cli-4.6.2.2472-linux.zip

# Copie tout le reste du code source dans le conteneur
COPY . .

# Définit le point d'entrée par défaut pour le conteneur
ENTRYPOINT ["sonar-scanner"]
