import globals from "globals";
import pluginJs from "@eslint/js";
import pluginReactConfig from "eslint-plugin-react/configs/recommended.js";
import { fixupConfigRules } from "@eslint/compat";

export default [
  {
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node,
      },
      ecmaVersion: 12,
      sourceType: "module",
    },
  },
  pluginJs.configs.recommended,
  ...fixupConfigRules(pluginReactConfig),
  {
    rules: {
      "constructor-super": "error",
      "no-duplicate-case": "error",
      "no-empty-pattern": "error",
      "no-undef": "off", // Disable no-undef for Node.js globals like require, module, etc.
      "no-unused-vars": ["warn", { args: "none" }],
    },
  },
  {
    settings: {
      react: {
        version: "detect", // Automatically detect the react version
      },
    },
  },
];
