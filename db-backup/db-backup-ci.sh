#!/bin/bash

### Variables in this section will need to be edited to reflect your configuration.
###### Start of section

# Your Linux user's home directory
export HOME=$CI_PROJECT_DIR/backup-files

# Your MongoDB's server hostname (SRV URI will handle this)
HOST=cluster0.jtw0ngm.mongodb.net

# The MongoDB database to be backed up
DBNAME=morning-news

# Linode Object Storage bucket name. Replace this value with the name of your Linode bucket and its associated region. This should be in the format {BUCKET_NAME}.{BUCKET_REGION}.linodeobjects.com
BUCKET=morning-news-bucket.fr-par-1.linodeobjects.com

# Linux user account
USER=root

# Linode Bucket Access Key. You can use an environment variable to store your Linode Access Key, or you can store it in plain text in this script in the line below. Plain text is not recommended for production environments.
LINODE_ACCESS_KEY=$LINODE_ACCESS_KEY
LINODE_SECRET_KEY=$LINODE_SECRET_KEY

###### End of section

# Current time and date
TIME=$(/bin/date +%d-%m-%Y-%T)

# Directory you'd like the MongoDB backup file to be saved to
DEST=$HOME/tmp

# Command to create a .tar file of the MongoDB backup files
TAR=$DEST/backup-$TIME.tar

# Command to create the backup directory (-p to avoid warning if the directory already exists)
/bin/mkdir -p $DEST

# Echo for logging purposes
echo "Backing up $HOST/$DBNAME to Linode $BUCKET on $TIME"

# Command to run the mongodump command that dumps all data for the specified database to the backup directory
mongodump --uri="mongodb+srv://$MONGO_USER:$MONGO_PASS@$HOST/$DBNAME?retryWrites=true&w=majority" --ssl --out $DEST

# Verify if mongodump was successful
if [ $? -ne 0 ]; then
    echo "mongodump command failed"
    exit 1
fi

# Create the .tar file of backup directory
tar -cvf $TAR -C $DEST .

# Verify if tar was successful
if [ $? -ne 0 ]; then
    echo "tar command failed"
    exit 1
fi

# Upload the .tar to Linode
/usr/local/bin/duck --username $LINODE_ACCESS_KEY --password $LINODE_SECRET_KEY --upload s3://$BUCKET/ $TAR

# Verify if the upload was successful
if [ $? -ne 0 ]; then
    echo "Upload to Linode bucket failed"
    exit 1
fi

# Log the end of the script
echo "Backup of MongoDB databases to Linode bucket $BUCKET completed successfully."
