const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../app');
const User = require('../models/users');

jest.setTimeout(30000); // Augmenter le délai d'attente à 30 secondes

const newUser = {
  username: 'test-lacapsule',
  password: 'test123',
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
  canBookmark: true,
};

// Utiliser les mocks de Jest
jest.mock('mongoose');
jest.mock('../models/users', () => {
  return function(data) {
    this.username = data.username;
    this.password = data.password;
    this.token = data.token;
    this.canBookmark = data.canBookmark;
    this._id = 'mocked_id';
    this.save = jest.fn().mockResolvedValue(this);
  };
});

beforeAll(async () => {
  await mongoose.connect();
});

beforeEach(() => {
  // Réinitialiser les mocks avant chaque test
  jest.clearAllMocks();
  User.findOne = jest.fn().mockResolvedValue(null);
  User.updateOne = jest.fn().mockResolvedValue({ n: 1, nModified: 1, ok: 1 });
  User.deleteOne = jest.fn().mockResolvedValue({ deletedCount: 1 });
});

it('Users schema & model', () => {
  expect(User).toBeDefined();

  const newFakeUser = new User(newUser);

  expect(newFakeUser).toHaveProperty('_id', 'mocked_id');
  expect(newFakeUser).toHaveProperty('username', newUser.username);
  expect(newFakeUser).toHaveProperty('password', newUser.password);
  expect(newFakeUser).toHaveProperty('token', newUser.token);
  expect(newFakeUser).toHaveProperty('canBookmark', newUser.canBookmark);
});

it('POST /users/signup', async () => {
  User.findOne.mockResolvedValueOnce(null); // Simuler qu'aucun utilisateur n'existe déjà

  const res = await request(app).post('/users/signup').send(newUser);

  expect(res.statusCode).toBe(200);
  expect(res.body.result).toBe(true);
  expect(res.body.token).toEqual(expect.any(String));
  expect(res.body.token.length).toBe(32);
});

 it('POST /users/signin', async () => {
   // Ajustement ici pour simuler un utilisateur existant avec les mêmes propriétés
   User.findOne.mockResolvedValueOnce(new User(newUser));

   const res = await request(app).post('/users/signin').send({
     username: newUser.username,
     password: newUser.password,
   });
  
   expect(res.statusCode).toBe(200);
 });

it('GET /users/canBookmark/:token', async () => {
  const res = await request(app).post('/users/signup').send(newUser);
  expect(res.statusCode).toBe(200);
  expect(res.body.result).toBe(true);
  expect(res.body.token).toEqual(expect.any(String));
  expect(res.body.token.length).toBe(32);

  await User.updateOne({ token: res.body.token }, { canBookmark: false });

  // Ajustement ici pour simuler un utilisateur mis à jour avec canBookmark à false
  const updatedUser = { ...newUser, canBookmark: false };
  User.findOne.mockResolvedValueOnce(updatedUser); 

  const res2 = await request(app).get(`/users/canBookmark/${res.body.token}`);
  expect(res2.statusCode).toBe(200);
  expect(res2.body.result).toBe(true);
  expect(res2.body.canBookmark).toBe(false);
});

afterAll(async () => {
  await mongoose.connection.close();
});
